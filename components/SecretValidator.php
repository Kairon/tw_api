<?php
/**
 * Created by PhpStorm.
 * User: ivakhnov
 * Date: 10/16/18
 * Time: 12:33 AM
 */

namespace app\components;


use yii\validators\Validator;

class SecretValidator extends Validator
{
    private $_username = '';

    public function validateAttribute($model, $attribute)
    {
        if(isset($model->username)){
            $this->_username = $model->username;
        }

        if(sha1($model->id . $this->_username) != $model->$attribute){
            $model->addError($attribute, 'access denied');
        }
    }
}