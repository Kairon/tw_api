<?php
/**
 * Created by PhpStorm.
 * User: ivakhnov
 * Date: 10/16/18
 * Time: 3:12 AM
 */

namespace app\components;

use Abraham\TwitterOAuth\TwitterOAuth;

class Twitter
{
    const CONSUMER_KEY = 'vbIAeJ8PYJxUhDuEHMsRnmbAQ';
    const CONSUMER_SECRET = 'rgeJTUM6OKHR72Gh2KYgFENQQsAexHRhUbjTsRU1neEcixjSpF';

    private $_connection;

    public function __construct()
    {
        $this->_connection = new TwitterOAuth(self::CONSUMER_KEY, self::CONSUMER_SECRET);
    }

    public function run($user = ''){
        return $this->_connection->get('statuses/user_timeline', ['count' => 1, 'exclude_replies' => true, 'screen_name' => $user, 'include_rts' => false]);

    }
}