<?php

namespace app\controllers;

use app\forms\AddTwUserForm;
use app\forms\RemoveUserForm;
use app\forms\TwittsForm;
use app\models\TwUser;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\rest\Controller;

class TwitterUserController extends Controller
{
    public function actionIndex(){
        return new ActiveDataProvider([
            'query' => TwUser::find(),
        ]);
    }

    public function actionAdd(){
        $model = new AddTwUserForm();
        if($model->load(\Yii::$app->request->get(), '')){
            return $model->addUser();
        }
        return ['error' => 'internal error'];
    }

    public function actionRemove(){
        $model = new RemoveUserForm();
        if($model->load(\Yii::$app->request->get(), '')){
            return $model->removeUser();
        }
        return ['error' => 'internal error'];
    }

    public function actionFeed(){
        $model = new TwittsForm();
        if($model->load(\Yii::$app->request->get(), '') && $model->run()){
            return ['feed' => $model->twittsArray];
        }
        return ['error' => 'internal error'];
    }

    protected function verbs()
    {
        return [
            '*' => ['GET']
        ];
    }
}
