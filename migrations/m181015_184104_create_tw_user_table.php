<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tw_user`.
 */
class m181015_184104_create_tw_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tw_user', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tw_user');
    }
}
