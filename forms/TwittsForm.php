<?php
/**
 * Created by PhpStorm.
 * User: ivakhnov
 * Date: 10/16/18
 * Time: 2:40 AM
 */

namespace app\forms;


use app\components\SecretValidator;
use app\components\Twitter;
use app\models\TwUser;
use yii\base\Model;

class TwittsForm extends Model
{
    public $id;
    public $secret;

    private $_array = [];

    public function rules()
    {
        return [
            [['id', 'secret'], 'required'],
            [['secret'], SecretValidator::className()]
        ];
    }

    public function run(){
        if($this->validate()){
            $this->setTwittsArray($this->getUsers());
            return true;
        }
        return false;
    }

    private function getUsers(){
        $arr = [];
        $users = TwUser::find()->asArray()->all();
        foreach ($users as $user){
            $arr[] = $user['username'];
        }
        return $arr;
    }

    private function setTwittsArray(array $users = []){
        $model = new Twitter();
        foreach ($users as $user){
            if($result = $model->run($user)){
                foreach ($result as $page){
                    $this->_array[]['user'] = $user;
                    $this->_array[]['tweet'] = $page->text;
                    $this->_array[]['hashtag'] = $page->entities->hashtags;
                }
            }
        }
    }

    public function getTwittsArray(){
        return $this->_array;
    }
}