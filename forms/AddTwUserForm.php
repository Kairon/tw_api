<?php
/**
 * Created by PhpStorm.
 * User: ivakhnov
 * Date: 10/16/18
 * Time: 12:20 AM
 */

namespace app\forms;


use app\components\SecretValidator;
use app\models\TwUser;
use yii\base\Model;

class AddTwUserForm extends Model
{
    public $id;
    public $username;
    public $secret;

    public function rules()
    {
        return [
            [['username', 'id', 'secret'], 'required'],
            [['username'], 'unique', 'targetClass' => TwUser::className()],
            //[['username'], 'unique', 'targetClass' => TwUser::className()],
            [['id'], 'string', 'max' => 32],
            [['secret'], SecretValidator::className()]
        ];
    }

    public function addUser(){
        if(!$this->validate(['username', 'id'])){
            return ['error' => 'Missing parameters'];
        }

        if($this->validate()){
            $this->createUser();
             return [];
        }

        return ['error' => 'Access denied'];
    }

    private function createUser(){
        $model = new TwUser([
            'username' => $this->username
        ]);
        return $model->save();
    }
}