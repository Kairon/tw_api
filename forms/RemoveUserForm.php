<?php
/**
 * Created by PhpStorm.
 * User: ivakhnov
 * Date: 10/16/18
 * Time: 2:18 AM
 */

namespace app\forms;


use app\components\SecretValidator;
use app\models\TwUser;
use yii\base\Model;

class RemoveUserForm extends Model
{
    public $id;
    public $username;
    public $secret;

    public function rules()
    {
        return [
            [['username', 'id', 'secret'], 'required'],
            [['id'], 'string', 'max' => 32],
            [['secret'], SecretValidator::className()]
        ];
    }

    public function removeUser(){
        if(!$this->validate(['id'])){
            return ['error' => 'Missing parameters'];
        }

        if($this->validate(['secret'])){
            $this->deleteUser();
            return [];
        }
        return ['error' => 'Access denied'];
    }

    private function deleteUser(){
        return TwUser::deleteAll(['username' => $this->username]);
    }
}